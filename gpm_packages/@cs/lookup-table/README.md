# README #

**composed-lookup-table** is a malleable by-reference API for storing and retrieving named values.

## Malleable VI Caveats and Warnings

*The level of malleablity required for this API only exists in LabVIEW 2017 **Service Pack 1** and later.* You will find no success using this package in LV2017 pre-service-pack or any prior LabVIEW version.

### How do I get set up?
- Install this package using [GPM](https://gpackage.io) and start coding
- All public API methods are clearly marked in the souce library

#### Examples
- No examples provided at this time

#### Dependencies
- This package depends only on base *vi.lib*
- If you want to run the unit tests, you must install JKI's VI Tester (you do not need to run the unit tests)

### Contribution guidelines
- All contributions must adhere to SOLID design principles.
- All code must be unit tested to the extent reasonably possible.
- Please contact the author if you want to contribute.

### Who do I talk to?
- Ethan Stern | Composed Systems, LLC
- ethan.stern@composed.io

### License
- See license file